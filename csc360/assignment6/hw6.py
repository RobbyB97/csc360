#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
@author:
Due date: Monday, October 15, 2018
"""

fName = 'Robby'
lName = 'Bergers'
courses = 4
coursees = ['CSC-360', 'CSC-341', 'CSC-380', 'CSC-123']
thelist = ['python', 'free', 'the']
coursedict = {'CSC-360': 'MWF 2-2:50PM', 'CSC-341': 'MWF 11-12', 'CSC-380': 'MTWTF 12-12', 'CSC-123': 'Only on weekends!'}

text = """
Python is an easy to learn, powerful programming language. It has efficient high-level data structures and a simple but effective approach to object-oriented programming. Python’s elegant syntax and dynamic typing, together with its interpreted nature, make it an ideal language for scripting and rapid application development in many areas on most platforms.

The Python interpreter and the extensive standard library are freely available in source or binary form for all major platforms from the Python Web site, https://www.python.org/, and may be freely distributed. The same site also contains distributions of and pointers to many free third party Python modules, programs and tools, and additional documentation.

The Python interpreter is easily extended with new functions and data types implemented in C or C++ (or other languages callable from C). Python is also suitable as an extension language for customizable applications.

This tutorial introduces the reader informally to the basic concepts and features of the Python language and system. It helps to have a Python interpreter handy for hands-on experience, but all examples are self-contained, so the tutorial can be read off-line as well.
"""


# 1.
def questionOne():
    print(fName, lName, "is taking " + str(courses) + " classes\n")
    return

# 2.
def questionTwo():
    print("The courses I am taking are:")
    for i in range(0, len(coursees)):
        print(str(i + 1) + ".  " + coursees[i])
    return

# 3.
def questionThree():
    realtext=text.lower()
    for i in range(0, len(thelist)):
        cmp = thelist[i]
        num = realtext.count(cmp)
        print("There are " + str(num) + " occurrences of " + str(cmp))

# 4.
def questionFour():
    thevar = input("What course are you searching for?")
    thevar.upper()
    if thevar in coursedict:
        print(coursedict[thevar])
    else:
        print("Course not found")

# 5.
def questionFive():
    elem1 = {
        "tag": "div",
        "id": "section1",
         "text": "This is section 1"
    }
    elem2 = {
        "tag": "div",
         "id": "information",
         "text": "Additional information"
    }
    elem3 = {
        "tag": "h1",
         "id": "heading",
         "text": "Welcome to my homepage"
    }
    masterelem=[elem1, elem2, elem3]
    for i in range(0, len(masterelem)):
        tag = masterelem[i]
        print("<" + tag["tag"] + " id='" + tag["id"] + "'> " + tag["text"] + " </" + tag["tag"] + ">")

if __name__ == '__main__':
    questionOne()
    questionTwo()
    questionThree()
    questionFour()
    questionFive()
